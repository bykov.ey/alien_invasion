import  sys, pygame


def check_events(ship):
    for event in pygame.event.get():
        if event.type   ==    pygame.QUIT:
            sys.exit()
        #проверка нажатия клавиш


        elif event.type == pygame.KEYDOWN:
            check_keydown_events(event, ship)

        elif event.type == pygame.KEYUP:
            check_keyup_events(event, ship)

def check_keydown_events(event, ship):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = True
    elif event.key == pygame.K_LEFT:
        ship.moving_left = True

def check_keyup_events(event, ship):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    elif event.key == pygame.K_LEFT:
        ship.moving_left = False


def update_screen(ai_settings, screen, ship):
    '''обновляет изображение на экране и отображает новый экран
при каждом проходе цикла перерисовывается экран'''
    screen.fill(ai_settings.bg_color)
    ship.blitme()

    #отображание последнего прорисованного экрана
    pygame.display.flip()